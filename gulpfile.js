'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var streamqueue  = require('streamqueue');
var autoprefixer = require('gulp-autoprefixer');
var uglifycss = require('gulp-uglifycss');
var minify = require('gulp-minify');
 
gulp.task('sass', function () {
  gulp.src('./css/scss/index.scss')
    .pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
    .pipe(concat('main.css'))
    .pipe(autoprefixer({
	    browsers: ['last 2 versions'],
	    cascade: false
	}))    
    .pipe(gulp.dest('./css'));
});

gulp.task('materialize', function () {
  gulp.src('./css/materialize/materialize.scss')
    .pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
    .pipe(concat('materialize.min.css'))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(uglifycss({
      "maxLineLen": 0,
      "uglyComments": true
    }))    
    .pipe(gulp.dest('./css'));
});


gulp.task('js-libs',function() {
  return streamqueue({ objectMode: true },
      gulp.src('./js/libs/materialize.min.js'),      
      gulp.src('./js/libs/slick.min.js'),
      gulp.src('./js/libs/jquery.truncate.min.js')
    )
    .pipe(concat('build-libs.js'))
    .pipe(gulp.dest('./js'));
});

gulp.task('js-source',function() {
  return streamqueue({ objectMode: true },
      gulp.src('./js/source/leaders-list.js'),
      gulp.src('./js/source/main.js')
    )
    .pipe(concat('build.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: [],
        ignoreFiles: []
    }))
    .pipe(gulp.dest('./js'));
});


gulp.task('sass:watch', function () {
  gulp.watch('./css/scss/**/*.scss', ['sass']);
});

gulp.task('materilie:watch', function () {
  gulp.watch('./css/materialize/**/*.scss', ['sass']);
});

gulp.task('default',function(){
  gulp.watch('./css/scss/**/*.scss', function(){
    setTimeout(function(){gulp.start('sass')},800);
  });
  gulp.watch('./js/source/**/*.js', ['js-source']);
  gulp.watch('./js/libs/**/*.js', ['js-libs']);  
});


gulp.task('build', ['sass','js-libs','js-source']);