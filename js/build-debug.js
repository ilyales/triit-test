function LeadersList() {
  var leaderPersonSelector = ".leader-person",
      showFullSelector = ".leader-person__title",
      sliderSelector = ".leader-person__items",
      commentSelector = ".leader-person__item-comment";

  init();

  function init() {
    
    $(leaderPersonSelector + ' ' + showFullSelector).each(function(){

    });

    $(leaderPersonSelector + ' ' + showFullSelector).on("click", function(){
      var slider = $(this).siblings('.leader-person__items');

      if (!$(this).hasClass('open')) {
        $(this).addClass('open');
        openSlider(slider);
      }
      else {
        $(this).removeClass('open');
        closeSlider(slider);
      }   
      
    });
  }

  function openSlider(_sliderEl) {
    _sliderEl.addClass('step1');
    initSlick(_sliderEl);
    truncateComment(_sliderEl.find(commentSelector));
    _sliderEl.addClass('step2');
    _sliderEl.addClass('step3');

   
  }

  function closeSlider(_sliderEl) {
    _sliderEl.slick('unslick');
    _sliderEl.removeClass('step1 step2 step3');
  }
 
  function initSlick(_element) {
    _element.slick({
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 1,
      prevArrow:'<img class="leader-person__items-arrow-left" src="img/arrow-left.png">',
      nextArrow:'<img class="leader-person__items-arrow-right" src="img/arrow-right.png">',

      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        }, 
        {
          breakpoint: 820,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
          }
        },    
      ]
    });
  }

  function truncateComment(_commentEl) {
    _commentEl.truncate({
      width: 'auto',
      token: '&hellip;',
      side: 'right',
      multiline: true
    });
  }
}
$(document).ready(function(){
  $(".button-collapse").sideNav();
  var leadersList = new LeadersList;
});